const hhtp = require("http");
const express = require("express");
const app = express();
app.set('view engine', "ejs");
const bodyparser = require("express");

app.use(express.static(__dirname + '/public'));

app.use(bodyparser.urlencoded({extended:true}));

let datos =[{
 matricula:"2020030806",
 nombre:"Francisco Vega Hernandez",
 sexo:"M",
 materia:["ingles","Tegnologia","Base de datos"],
},{
    matricula:"2020020309",
    nombre:"ALMOGAR VAZQUEZ YARLEN DE JESUS",
    sexo:"F",
    materia:["ingles","Tegnologia","Base de datos"],
},{
    matricula:"2020020001",
    nombre:"ACOSTA ORTEGA JESUS HUMBERTO",
    sexo:"M",
    materia:["ingles","Tegnologia","Base de datos"],

},{
    matricula:"2020020890",
    nombre:"LUIS GUILLERMO CHAVEZ ORTIZ",
    sexo:"M",
    materia:["ingles","Tegnologia","Base de datos"],
},{
    matricula:"2020020721",
    nombre:"MARIANA DE JESUS HERNANDEZ COLIO",
    sexo:"F",
    materia:["ingles","Tegnologia","Base de datos"],
},{
    matricula:"2020020691",
    nombre:"MAKAKARDO VEGA HERNANDEZ",
    sexo:"M",
    materia:["ingles","Tegnologia","Base de datos"],
}

]


app.get('/',(req,res)=>{
    
    res.render('index',{titulo:"Listado de Alumnos",listado:datos})
    //res.send("<h1>Iniciamos con express<h1>");

})

app.get("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }
    res.render('cotizacion', valores);
})

app.post("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion', valores);
})

// escuchar comentarios dels evidor por el puerto 3000

const puerto = 3000;


app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/views/error.html')
})
app.listen(puerto,()=>{0

    console.log("Iniciado puerto 3000");
    
})